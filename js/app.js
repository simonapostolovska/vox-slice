// $(".chosen-select").chosen({
//   width: "100%",
//   disable_search: true,
// });


// if ($(window).width() <= 992) {
//   $(".partners-slider").slick({
//     dots: false,
//     arrows: false,
//     // mobileFirst: true,
//     slidesToShow: 3,
//     slidesToScroll: 1,
//   });

//   $(".article-story-wrapper").slick({
//     dots: true,
//     arrows: false,
//     // mobileFirst: true,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//   });

//   $(".slider-mobile").slick({
//     dots: true,
//     arrows: false,
//     // mobileFirst: true,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//   });

//   $(".jobs-list-cards").slick({
//     dots: true,
//     arrows: false,
//     centerMode: true,
//     centerPadding: '30px',
//     // mobileFirst: true,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//   });
// }

// $(".big-slider-inner").slick({
//   dots: false,
//   arrows: true,
//   // mobileFirst: true,
//   slidesToShow: 1,
//   slidesToScroll: 1,
// });

// $(".country-page-gallery").slick({
//   dots: false,
//   arrows: false,
//   centerMode: true,
//     centerPadding: '10%',
//   // mobileFirst: true,
//   slidesToShow: 2,
//   slidesToScroll: 1,
// });

// $(".related-projects-wrapper").slick({
//   dots: false,
//   arrows: true,
//   // mobileFirst: true,
//   slidesToShow: 3,
//   slidesToScroll: 1,
// });

// // MENU

// $('.menu-toggle').on('click', function () {
//   $(this).toggleClass('menu-toggle-open');
//   $('.header').toggleClass('header-menu-open');
//   $('body').toggleClass('scroll-fixed');
// });

// // // GENERAL PURPOSE OFFSET CALCULATOR TO STICK ELEMENT TO WINDOW BORDER

// function throttle(callback, limit) {
//   let wait = false;
//   const context = this;
//   return function (e) {
//     if (!wait) {
//       callback.call(context, e);
//       wait = true;

//       setTimeout(function () {
//         wait = false;
//       }, limit);
//     }
//   };
// }

// $.fn.offsetToWindowBorder = function (direction, wrapper = 'container', wrapperPadding = 12) {
//     function offsetFn () {
//       const wrapperWidth = document.getElementsByClassName(wrapper)[0].offsetWidth;
//       const screenWidth = document.body.clientWidth;
//       const offset = Math.floor((wrapperWidth - (screenWidth + 2*wrapperPadding)) / 2)
      
//       $(this).find('.wall-stuck-inner').css({ [direction]: offset });
//     }

//     const boundOffsetFn = offsetFn.bind($(this));
//     const throttledOffsetFn = throttle(boundOffsetFn, 100)

//     boundOffsetFn();

//     $(window).on('resize', function() {
//       throttledOffsetFn();
//     })

//     return $(this);
// }

// const stuckToRightWall = $('.wall-stuck-right');
// const stuckToLeftWall = $('.wall-stuck-left');
// stuckToRightWall.offsetToWindowBorder('right');
// stuckToLeftWall.offsetToWindowBorder('left');


$(function () {
  $(".chosen-select").chosen({
      width: "100%",
      disable_search: true,
  });


if ($(window).width() <= 992) {
  $(".partners-slider").slick({
    dots: false,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
  });

  $(".article-story-wrapper").slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  });

  $(".slider-mobile").slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  });

  $(".jobs-list-cards").slick({
    dots: true,
    arrows: false,
    centerMode: true,
    centerPadding: '30px',
    slidesToShow: 1,
    slidesToScroll: 1,
  });
}

  $(".big-slider-inner").slick({
    dots: false,
    arrows: true,
    // mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  });

  $(".country-page-gallery").slick({
      dots: false,
      arrows: false,
      centerMode: true,
      centerPadding: '10%',
      slidesToShow: 2,
      slidesToScroll: 1,
  });
  
  $(".split-slider").slick({
      dots: true,
      arrows: false,
      centerPadding: '10%',
      slidesToShow: 1,
      slidesToScroll: 1,
  });

  $(".related-projects-wrapper").slick({
      arrows: false,
      infinite: false,  
      dots: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 941,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    
  });
  
  $(".services-slider").slick({
      arrows: false,
      infinite: false,  
      dots: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 941,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    
  });
  $(".tech-slider").slick({
      arrows: false,
      infinite: false,  
      dots: false,
      slidesToShow: 3,
      // slidesPerRow: 3,
      slidesToScroll: 1,
      adaptiveHeight: true,
      rows:2,
      responsive: [
        {
          breakpoint: 941,
          settings: {
            slidesToShow: 1.2,
            slidesToScroll: 1,
            rows:1,
            dots:true
          }
        }
      ]
    
  });
  $(".methods-slider").slick({
      arrows: false,
      infinite: false,  
      dots: false,
      slidesToShow: 2,
      // slidesPerRow: 3,
      slidesToScroll: 1,
      adaptiveHeight: true,
      rows:2,
      responsive: [
        {
          breakpoint: 941,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            rows:1,
            dots:true
          }
        }
      ]
    
  });

  // MENU
  $('.menu-toggle').on('click', function () {
      $(this).toggleClass('menu-toggle-open');
      $('.header').toggleClass('header-menu-open');
      $('body').toggleClass('scroll-fixed');
  });

  // // GENERAL PURPOSE OFFSET CALCULATOR TO STICK ELEMENT TO WINDOW BORDER

  function throttle(callback, limit) {
      let wait = false;
      const context = this;
      return function (e) {
          if (!wait) {
              callback.call(context, e);
              wait = true;

              setTimeout(function () {
                  wait = false;
              }, limit);
          }
      };
  }

  $.fn.offsetToWindowBorder = function (direction, wrapper = 'container', wrapperPadding = 12) {
    function offsetFn () {
      const marginDirection = `margin-${direction}`;
      const screenWidth = document.body.clientWidth;
      const sticker = $(this).find('.wall-stuck-inner');

      if (screenWidth <= 991) {
        sticker.css({ [marginDirection]: 0 });
        return;
      }

      const wrapperWidth = document.getElementsByClassName(wrapper)[0].offsetWidth;
      const offset = Math.floor((wrapperWidth - (screenWidth + 2 * wrapperPadding)) / 2)
      
      sticker.css({ [marginDirection]: offset });
    }

    const boundOffsetFn = offsetFn.bind($(this));
    const throttledOffsetFn = throttle(boundOffsetFn, 100)

    boundOffsetFn();

    $(window).on('resize', function() {    
      throttledOffsetFn();
    });

    return $(this);
  }


  const stuckToRightWall = $('.wall-stuck-right');
  const stuckToLeftWall = $('.wall-stuck-left');
  stuckToRightWall.offsetToWindowBorder('right');
  stuckToLeftWall.offsetToWindowBorder('left');


  ////////////////////////////////////////////////////////////////
  // expand ellipted text
  ////////////////////////////////////////////////////////////////

  $('.clients-box-more').on('click', function(){
    let elliptedTextWrapper = $(this).parent().parent().children('.clients-box-text-wrapper');
    let elliptedText = elliptedTextWrapper.children('.clients-box-text');

    elliptedTextWrapper.removeClass('ellipted');
    elliptedText.removeClass('ellipted')

    $(this).hide();
    $(this).parent().children('.clients-box-less').show();
  });

  $('.clients-box-less').on('click', function(){
    let elliptedTextWrapper = $(this).parent().parent().children('.clients-box-text-wrapper');
    let elliptedText = elliptedTextWrapper.children('.clients-box-text');

    elliptedTextWrapper.addClass('ellipted');
    setTimeout(function() {
      elliptedText.addClass('ellipted')
    }, 250);

    $(this).hide();
    $(this).parent().children('.clients-box-more').show();
  });

  ////////////////////////////////////////////////////////////////
  // OFFSET VERTICALLY ELEMENT
  ////////////////////////////////////////////////////////////////

  const underslideElements = $('[data-underslide-id]');
  
  function initUnderslide () {
    for(ele of underslideElements) {
      const winWidth = window.innerWidth;
      const eleId = ele.dataset.underslideId;
      const eleOffsetVal = ele.dataset.underslideOffset;
      const eleHeight = ele.offsetHeight;
      const eleUnderslideIgnition = ele.dataset.underslideIgnition;
      const mobileFirst = ele.dataset.underslideMobileFirst;
      const offsetPush = document.querySelector(`[data-underslide-push='${eleId}']`);
      const offsetPull = document.querySelector(`[data-underslide-pull='${eleId}']`);
      const sizeComparison = !!mobileFirst ? winWidth > eleUnderslideIgnition : winWidth < eleUnderslideIgnition;

      if(!!eleUnderslideIgnition && sizeComparison) {
        ele.style.transform = 'translateY(0)';
        if( !!offsetPush ) offsetPush.style.paddingTop = 0;
        if( !!offsetPull ) offsetPull.style.marginBottom = 0;

        continue;
      }

      ele.style.transform = `translateY(${eleOffsetVal}%)`;
      if( !!offsetPush ) offsetPush.style.paddingTop = `${eleHeight * (eleOffsetVal / 100)}px`;
      if( !!offsetPull ) offsetPull.style.marginBottom = `-${eleHeight * (eleOffsetVal / 125)}px`;
    }
  }

  initUnderslide();
  const throttledUnderslide = throttle(initUnderslide, 100);

  $(window).on('resize', function() {    
    throttledUnderslide();
  });

});

